/*jshint node: true */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  win: {
    type: Number,
    required: true
  },
  draw: {
    type: Number,
    required: true
  },
  lose: {
    type: Number,
    required: true
  },
  streak: {
    type: Number,
    required: true
  },
  points: {
    type: Number,
    required: true
  }
});

module.exports = mongoose.model('User', userSchema);