/*jshint jquery: true, browser: true, devel: true, globalstrict: true */

'use strict';

var fillScores = function (marks, player) {
  
  var mark;
  for (mark in marks) {
    if (marks.hasOwnProperty(mark)) {
      var selector = '#p' + player + '-' + mark;
      var $cell = $(selector);
      
      if ('sum1' !== mark && 'sum2' !== mark) {
        if (!$cell.attr('selected')) {
          $cell.text(marks[mark]);
          $cell.css('color', '#b4b4b4');
        }
      }
    }
  }
};