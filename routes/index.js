/*jshint node: true */

exports.index = function (req, res) {
  res.render('index');
};

exports.noEntry = function (req, res) {
  res.render('noEntry');
};